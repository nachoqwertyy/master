<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "placashorarios".
 *
 * @property int $ID
 * @property int $ID_PLACAS
 * @property int $ID_HORARIOS
 *
 * @property Placas $pLACAS
 * @property Horarios $hORARIOS
 */
class Placashorarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'placashorarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_PLACAS', 'ID_HORARIOS'], 'required'],
            [['ID_PLACAS', 'ID_HORARIOS'], 'integer'],
            [['ID_PLACAS'], 'exist', 'skipOnError' => true, 'targetClass' => Placas::className(), 'targetAttribute' => ['ID_PLACAS' => 'ID']],
            [['ID_HORARIOS'], 'exist', 'skipOnError' => true, 'targetClass' => Horarios::className(), 'targetAttribute' => ['ID_HORARIOS' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ID_PLACAS' => 'Id Placas',
            'ID_HORARIOS' => 'Id Horarios',
        ];
    }

    /**
     * Gets query for [[PLACAS]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPLACAS()
    {
        return $this->hasOne(Placas::className(), ['ID' => 'ID_PLACAS']);
    }

    /**
     * Gets query for [[HORARIOS]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHORARIOS()
    {
        return $this->hasOne(Horarios::className(), ['ID' => 'ID_HORARIOS']);
    }
    
    public function getPlacaHorario($id_placas){                     
        
        $id = Placashorarios::find()->where(['ID_PLACAS'=>$id_placas])->asArray()->all();
                
        return $id;        
        
    }
    
}
