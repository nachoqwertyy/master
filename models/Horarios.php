<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "horarios".
 *
 * @property int $ID
 * @property string|null $hora_inicio
 * @property string|null $hora_fin
 *
 * @property Placashorarios[] $placashorarios
 */
class Horarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'horarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hora_inicio', 'hora_fin'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'hora_inicio' => 'Hora Inicio',
            'hora_fin' => 'Hora Fin',
        ];
    }

    /**
     * Gets query for [[Placashorarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlacashorarios()
    {
        return $this->hasMany(Placashorarios::className(), ['ID_HORARIOS' => 'ID']);
    }
    
    public function getHorarioProhibido($id, $hora_inicio, $hora_fin){                     
        
        $res = Horarios::find()->where(['ID'=>$id])
                ->andWhere("hora_inicio<:p_hora_inicio",array(':p_hora_inicio'=>$hora_inicio))
                ->andWhere("hora_fin>:p_hora_fin",array(':p_hora_fin'=>$hora_fin))
                ->asArray()->all();
                
        return $res;
        
    }
}
