<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "placas".
 *
 * @property int $ID
 * @property int|null $ultimo_digito
 * @property string $dia
 *
 * @property Placashorarios[] $placashorarios
 */
class Placas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'placas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ultimo_digito'], 'integer'],
            [['dia'], 'required'],
            [['dia'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ultimo_digito' => 'Ultimo Digito',
            'dia' => 'Dia',
        ];
    }

    /**
     * Gets query for [[Placashorarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlacashorarios()
    {
        return $this->hasMany(Placashorarios::className(), ['ID_PLACAS' => 'ID']);
    }
    
    public function getPlaca($placa, $dia){                     
        
        $id = Placas::find()->where(['ultimo_digito'=>$placa])->andWhere(['dia'=>$dia])->asArray()->all();
                
        return $id;        
        
    }
}
