<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use yii\base\Model;

/**
 * Description of FormularioPicoPlaca
 *
 * @author DESARROLLO-NACHO
 */
class EntryForm extends Model {
    public $placa;
    public $fecha;
    public $hora;
    public $resultado;

    public function rules()
    {
        return [
            [['placa', 'fecha', 'hora'], 'required']
        ];
    }
    
}
