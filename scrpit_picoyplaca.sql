CREATE DATABASE  IF NOT EXISTS `picoyplaca` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `picoyplaca`;
-- MySQL dump 10.13  Distrib 5.7.21, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: picoyplaca
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `horarios`
--

DROP TABLE IF EXISTS `horarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `horarios` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `hora_inicio` time DEFAULT NULL,
  `hora_fin` time DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horarios`
--

LOCK TABLES `horarios` WRITE;
/*!40000 ALTER TABLE `horarios` DISABLE KEYS */;
INSERT INTO `horarios` VALUES (1,'07:00:00','09:30:00'),(2,'16:00:00','19:30:00');
/*!40000 ALTER TABLE `horarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `placas`
--

DROP TABLE IF EXISTS `placas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `placas` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ultimo_digito` int(11) DEFAULT NULL,
  `dia` varchar(25) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `placas`
--

LOCK TABLES `placas` WRITE;
/*!40000 ALTER TABLE `placas` DISABLE KEYS */;
INSERT INTO `placas` VALUES (1,1,'Monday'),(2,2,'Monday'),(3,3,'Tuesday'),(4,4,'Tuesday'),(5,5,'Wednesday'),(6,6,'Wednesday'),(7,7,'Thursday'),(8,8,'Thursday'),(9,9,'Friday'),(10,0,'Friday');
/*!40000 ALTER TABLE `placas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `placashorarios`
--

DROP TABLE IF EXISTS `placashorarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `placashorarios` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_PLACAS` int(11) NOT NULL,
  `ID_HORARIOS` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_terminacion_placas` (`ID_PLACAS`),
  KEY `fk_intervalo_horarios` (`ID_HORARIOS`),
  CONSTRAINT `placashorarios_ibfk_1` FOREIGN KEY (`ID_PLACAS`) REFERENCES `placas` (`ID`),
  CONSTRAINT `placashorarios_ibfk_2` FOREIGN KEY (`ID_HORARIOS`) REFERENCES `horarios` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `placashorarios`
--

LOCK TABLES `placashorarios` WRITE;
/*!40000 ALTER TABLE `placashorarios` DISABLE KEYS */;
INSERT INTO `placashorarios` VALUES (1,1,1),(2,2,1),(3,3,1),(4,4,1),(5,5,1),(6,6,1),(7,7,1),(8,8,1),(9,9,1),(10,10,1),(11,1,2),(12,2,2),(13,3,2),(14,4,2),(15,5,2),(16,6,2),(17,7,2),(18,8,2),(19,9,2),(20,10,2);
/*!40000 ALTER TABLE `placashorarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-03 15:32:29
