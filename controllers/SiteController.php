<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\EntryForm;
use app\models\Placas;
use app\models\Placashorarios;
use app\models\Horarios;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

    public function actionEntry() {
        $model = new EntryForm;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            // validar los datos recibidos en el modelo

            $model->resultado = $this->validacion($model->placa, $model->fecha, $model->hora);

            // devuelvo resultado al modelo
            return $this->render('entry-confirm', ['model' => $model]);
        } else {
            // la página es mostrada inicialmente o hay algún error de validación
            return $this->render('entry', ['model' => $model]);
        }
    }

    private function validacion($placa, $fecha, $hora) {
        $placas = new Placas;
        $res = $placas->getPlaca(substr($placa, -1), $this->getDayOfDate($fecha));

        if (sizeof($res) == 0) {
            $res = Yii::$app->params['puede'];
        } else {
            $placas_horarios = new Placashorarios;

            $res_ph = $placas_horarios->getPlacaHorario(($res[0]['ID']));
           
            $hit = false;
            
            foreach ($res_ph as $horario) {
                $horarios = new Horarios;
                $res_hp = $horarios->getHorarioProhibido($horario['ID_HORARIOS'], $hora, $hora);
                if (sizeof($res_hp) > 0) {
                    $hit = true;
                }
            }

            if ($hit){
                $res = Yii::$app->params['noPuede'];
            }else{
                $res = Yii::$app->params['puede'];
            }
        }        

        return $res;
    }

    private function getDayOfDate($date) {
        $unixTimestamp = strtotime($date);
        $dayOfWeek = date("l", $unixTimestamp);
        return $dayOfWeek;
    }

}
