<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'puede' => 'Puede circular',
    'noPuede' => 'No puede circular'
];
