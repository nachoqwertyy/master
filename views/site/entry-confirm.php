<?php
use yii\helpers\Html;
$this->title = 'Validar Placa';
$this->params['breadcrumbs'][] = $this->title;
?>
<p>Resultado de la predicción de Pico y Placa:</p>


<ul>
    <li><label>Placa</label>: <?= Html::encode($model->placa) ?></li>
    <li><label>Fecha</label>: <?= Html::encode($model->fecha) ?></li>
    <li><label>Hora</label>: <?= Html::encode($model->hora) ?></li>
    <li><label>Resultado</label>: <?= Html::encode($model->resultado) ?></li>
</ul>