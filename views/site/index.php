<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Pico & Placa Predictor</h1>

        <p class="lead">Bienvenido a la app web que sirve para predecir si usted podrá conducir o no su vehículo en la ciudad de Quito</p>        
        
        <p class="lead">Para comenzar a utilizar la aplicación haga clic en el siguiente botón:</p>
        
        <?= Html::a('Configurar', ['/site/about'], ['class'=>'btn btn-primary']) ?>
    </div>
    
</div>
