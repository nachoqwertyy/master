<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Instrucciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
       Para utilizar correctamente esta aplicación es necesario configurar los siguientes apartados:
              
    </p>
    
    <p>1- Alimentar el maestro <?= Html::a('Placas', ['/placas/index'], ['class'=>'text']) ?> donde se debe ingresar el último dígito de la placa y el día que no puede circular, por ejemplo 1 - Monday</p>

    <p>2- Registrar en el maestro <?= Html::a('Horarios', ['/horarios/index'], ['class'=>'text']) ?> la información sobre la restricción de horas durante la semana y el horario del fin de semana: 05:00:00 - 20:00:00</p>
    
    <p>3- En el maestro <?= Html::a('Placas y Horarios', ['/placashorarios/index'], ['class'=>'text']) ?> tenemos la relación entre las tablas Placas y Horarios, información necesaria para establecer las relaciones entre Placas, días y horarios del Pico y Placa.</p>
    
    <p>4- Una vez que toda la información se encuentre parametrizada debe dirigirse a la sección <?= Html::a('Realizar Predicción', ['/site/entry'], ['class'=>'text']) ?>, allí se le solicitarán unos datos y la aplicación podrá predecir si usted puede circular o no con su vehículo </p>
</div>
