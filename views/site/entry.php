<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use janisto\timepicker;

$this->title = 'Realizar Predicción';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'placa') ?>


    <?php echo $form->field($model, 'fecha')->widget(\janisto\timepicker\TimePicker::className(), [
            'mode' => 'date',
            'clientOptions'=>[
                'dateFormat' => 'yy-mm-dd',
                'timeFormat' => 'HH:mm:ss',
                'showSecond' => false,
            ]
        ]); ?>

    <?php echo $form->field($model, 'hora')->widget(\janisto\timepicker\TimePicker::className(), [
        //'language' => 'fi',
        'mode' => 'time',
        'clientOptions'=>[
            'timeFormat' => 'HH:mm:ss',
            'showSecond' => true,
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>



<?php ActiveForm::end(); ?>