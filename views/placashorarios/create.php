<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Placashorarios */

$this->title = 'Create Placas y horarios';
$this->params['breadcrumbs'][] = ['label' => 'Placashorarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="placashorarios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
