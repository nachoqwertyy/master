<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PlacashorariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Placas y Horarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="placashorarios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Placashorarios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'ID_PLACAS',
            'ID_HORARIOS',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
