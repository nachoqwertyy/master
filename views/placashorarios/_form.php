<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Placashorarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="placashorarios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID_PLACAS')->textInput() ?>

    <?= $form->field($model, 'ID_HORARIOS')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
