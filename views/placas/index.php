<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PlacasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Placas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="placas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Placas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'ultimo_digito',
            'dia',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
