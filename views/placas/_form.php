<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Placas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="placas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ultimo_digito')->textInput() ?>

    <?= $form->field($model, 'dia')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
