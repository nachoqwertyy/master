<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Placas */

$this->title = 'Create Placas';
$this->params['breadcrumbs'][] = ['label' => 'Placas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="placas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
